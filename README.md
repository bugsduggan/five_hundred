# five hundred

This is a micro-service to return 500 errors.

To run (assuming you've got pipenv installed):
```
  pipenv install
  pipenv shell
  ./manager runserver
```

This has been tested (for all of 5 minutes) with python 3.5
