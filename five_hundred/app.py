from flask import Flask


def create_app(config=None):
    app = Flask(__name__)
    from five_hundred.resource import api
    api.init_app(app)
    return app
