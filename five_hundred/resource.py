from flask_restful import Api, Resource, abort


api = Api()


class FiveHundredResource(Resource):

    def get(self, placeholder=None):
        abort(500)

    def put(self, placeholder=None):
        abort(500)

    def post(self, placeholder=None):
        abort(500)

    def patch(self, placeholder=None):
        abort(500)

    def delete(self, placeholder=None):
        abort(500)


api.add_resource(FiveHundredResource, '/', '/<string:placeholder>')
